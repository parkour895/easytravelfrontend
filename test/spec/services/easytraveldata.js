'use strict';

describe('Service: easyTravelData', function () {

  // load the service's module
  beforeEach(module('easyTravelFrontEndApp'));

  // instantiate service
  var easyTravelData;
  beforeEach(inject(function (_easyTravelData_) {
    easyTravelData = _easyTravelData_;
  }));

  it('should do something', function () {
    expect(!!easyTravelData).toBe(true);
  });

});
