'use strict';

/**
 * @ngdoc function
 * @name easyTravelFrontEndApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the easyTravelFrontEndApp
 */
angular.module('easyTravelFrontEndApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
