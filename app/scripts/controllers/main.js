'use strict';

/**
 * @ngdoc function
 * @name easyTravelFrontEndApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the easyTravelFrontEndApp
 */
angular.module('easyTravelFrontEndApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
