'use strict';

/**
 * @ngdoc service
 * @name easyTravelFrontEndApp.easyTravelData
 * @description
 * # easyTravelData
 * Service in the easyTravelFrontEndApp.
 */
angular.module('easyTravelFrontEndApp')
  .service('easyTravelData', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var getOptimalWay=function (trip) {
      return $http.post('https://localhost:8083/optimalWay',trip,{
        headers: {
          'Content-Type' : 'application/json',
          'Access-Control-Allow-Origin': '*'
        }
      }).then(function (way) {
        console.log('@@@HomeCtrl: success getOptimalWay ');
        return way.data;
      });
    };

    return {
      getOptimalWay: getOptimalWay
    };
  });
