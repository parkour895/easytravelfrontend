##Front-end part of my Graduate work.

Multitravel web app, which include Google Maps Api, Skyscanner Api and Aviation Edge Api.

![Scheme](images/1.jpg)
![Scheme](images/2.jpg)
![Scheme](images/3.jpg)

# easy-travel-front-end

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

AngularJs 1.4  
Bootstrap 3.2.0 
Angular Material 1.1.7

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.